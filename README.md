# README #

CAR-T study with Carl June's gropu at UPENN.

### What is this repository for? ###

Experiment: Three groups of CAR-T cells bearing different costimulatory domains, including ICOS, CD28 and a CD28mutant that contains one of the signaling motifs for ICOS. See the abstract we submitted to ASGCT attached for more information. In this experiment, we treated animals with tumors with CAR-T cells and isolated the T-cells from tumors at 7 and 14 after treatment for RNAseq. There are 3-4 animals per time-point and group.

    1. Data showed that the three groups of CAR-T cells have a similar expression profile on day 7, but these CAR-T cells are pretty different among them on day 14. In order to show this information, I would like the first figure to include an MDS analysis of the differentially expressed genes between the 3 sets of CAR-T cells, including the total number of differentially expressed genes among groups (28z, 28zmut, ICOSz) at 7 and 14 days, similar to what you did in our previous paper with Th17 cells (see Figure 1 attached)

    2. Can you do Gene set enrichment analysis (GSEA) or ssGSEA for the attached lists of genes on samples from day 14 (28z, 28zmut and ICOSz)? I am attaching a list of genes relevant in T cell differentiation (effector cells) and exhaustion.

    3. Can you prepare a heat map with the following list of DEG for day 14 including 28z, 28zmut and ICOSz in this order? Not sure if it would be better to show the 3-4 animals per group (we would have around 10 columns) or we should just show the mean for each (so, three columns). What do you think is better?

    4. BGI prepared some scatter plots that look good. If I give you a short list of genes, would it be possible to indicate which point in the Day14_28z-vs-28zmutA.Ebseq_Method scatter plot correspond to each gene (see Figure 2 as example)? If it’s too complicated, we could probably just go with the heat map.

